# PROJET FIL ROUGE ENTREPRISE INSPECTOR

Nous sommes sollicités par une entreprise industrielle de contrôle de qualité de pièces mécaniques. Cette entreprise
a démarré l'an dernier un projet d'application permettant de lister toutes les pièces concernées et leurs
caractéristiques associées, puis de diagnostiquer dans les locaux industriels l'état d'usure de ces pièces. Le
cabinet qui a été consulté pour produire l'étude et la conception de cette application a dû se retirer du projet et
n'a pas pu produire l'ensemble de l'étude.

---

**Membres de l'équipe** :

- *Myriam* 
- *Zahra*
- *Rime*


Dans un premier temps nous mettrons en place les diagrammes UML.

___
## UML
___

### ***Diagramme de cas d'utilisation***

#### **Auditeur**   

![use case auditeur](UML/CasUtilisation/_useCaseAuditeur.jpeg)

#### **Responsable sécurité**
![use case Responsable sécurité](UML/CasUtilisation/_useCaseResponsableSecurite.jpeg)

#### **Chargé de référentiel**
![use case chargé de référentiel](UML/CasUtilisation/_useCaseChargeReferentiel.jpeg)

#### **Chef de projet**
![usecase Chef de projet](UML/CasUtilisation/_useCaseChefProjet.jpeg)

### ***Diagramme d'activité***

#### **Audit**
![activité audit](UML/Activite/activiteAudit.png)

#### **Création de campagne** 
![activité création camp](UML/Activite/activiteCreationCampagne.png)

#### **Gestion des campagne**
![activité gestion camp](UML/Activite/activiteGestionDeCampagne.png)

#### **Référentiel materiel**
![activité référentiel mat](UML/Activite/activiteMat%C3%A9riels.png)

#### **Référentiel pièce**
![activité référentiel piece](UML/Activite/activitePieces.png)

#### **Statistique** 
![activité stat](UML/Activite/activiteStatistique.png)


___
## Modèle Conceptuel de Données 
___

**Documentation**

**Entités et Attributs**

### Entité `users`

    Cette entité contient les utilisateurs

| Attribut    | Description   |
|----------------|-----------|
| name        | nom de l'utilisateur| 
| password       | mot de passe de l'utilisateur| 

### Entité `roles`

    Cette Entité contient les roles 

| Attribut       | Description | 
|----------------|-------------|
| role           | nom du role| 

### Entité `sites`

    Cette Entité contient les sites


| Attribut    | Description | 
|----------------|----------|
| name        | nom du site | 
| adress      | adresse du site| 

### Entité `campaigns`

    Cette Entité contient les campagnes


| Attribut    | Description | 
|----------------|-------------|
| name        | nom de la campagne|
| status      | status de la campagne|

### Entité `types`

    Cette Entité contient les types de pièces ou materiels

| Attribut    | Description | 
|----------------|-------------|
| nom         | nom du type|

### Entité `constructors`

    Cette Entité contient les constructeurs des pièces ou des matériels


| Attribut    | Description | 
|-------------|-------------|
| name        | nom du constructeur |

### Entité `models`

    Cette Entité contient les modeles de pièces et de matèriels

| Attribut    | Description | 
|----------------|------------- |
| name            | nom du modèle|
| creation_year   | année de création|
| status          | status du modèle |
| has_electro     | si il y a de l'électronique|

### Entité `pieces`

    Cette Entité contient les pièces


| Attribut    | Description | 
|----------------|-------------|
| date_mer       | date de mise en route|   
| status_fonctionnel | status du fonctionnement|

### Entité `materials`

    Cette Entité contient les matériels


| Attribut    | Description | 
|----------------|-------------|
| name           | nom du materiel|
| description    | description du materiel       |

![MCD inspector](/Basededonnées/mcd/mcd.png)
___

## Modèle Logique de Données   
___

### Table `users`

    Cette contient les utilisateurs

| Attribut    | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:-------:|
| id          | int         | ✓           |             |        |
| name        | varchar(55)|             |             | ✓      |
| password        | varchar(255)|             |             |        |


### Table `roles`

    Cette table contient les roles 

| Attribut       | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:-------:|
| id             | int         | ✓           |             |        |
| role          | varchar(255)|             |             | ✓      |

### Table `sites`

    Cette table contient les sites


| Attribut    | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:-------:|
| id          | int         | ✓           |             |        |
| name        | varchar(50)|             |             | ✓      |
| adress    | varchar(255)|             |             |        |

### Table `campaigns`

    Cette table contient les campagnes


| Attribut    | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:-------:|
| id          | int         | ✓           |             |        |
| name        | varchar(255)|             |             | ✓      |
| status      | varchar(50)|             |             |        |
| auditor_id  | int         |             | ✓           |        |
| creator_id  | int         |             | ✓           |        |
| site_id     | int         |             | ✓           |        |

### Table `types`

    Cette table contient les types de pièces ou materiels

| Attribut    | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:-------:|
| id          | int         | ✓           |             |        |
| nom         | varchar(50)|             |             | ✓      |

### Table `constructors`

    Cette table contient les constructeurs des pièces ou des matériels


| Attribut    | Type        | Primary Key | Foreign Key | Unique |
|-------------|-------------|:-----------:|:-----------:|:------:|
| id          | int         | ✓           |             |        |
| name        | varchar(50) |             |             | ✓      |

### Table `models`

    Cette table contient les modeles de pièces et de matèriels

| Attribut        | Type        | Primary Key | Foreign Key | Unique |
|----------------|------------- |:-----------:|:-----------:|:-------:|
| id              | varchar(255) | ✓           |             |        |
| name            | varchar(255)|             |             | ✓      |
| creation_year   | int         |             |             |        |
| status          | varchar(50) |             |             |        |
| has_electro     | int         |             |             |        |
| constructor_id  | int         |             | ✓           |        |
| type_id         | int         |             | ✓           |        |
| compose         | varchar(255)|             |   ✓         |        |

### Table `pieces`

    Cette table contient les pièces


| Attribut       | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:-------:|
| id             | int         | ✓           |             |        |
| date_mer       | date        |             |             |        |
| status_fonctionnel | varchar(50)|          |             |        |
| material_id    | int         |             | ✓           |        |
| model_id       | int         |             | ✓           |        |

### Table `materials`

    Cette table contient les matériels


| Attribut       | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:-------:|
| id             | int         | ✓           |             |        |
| name           | varchar(50)|             |             | ✓      |
| description    | text        |             |             |        |

### Table `compatible_models`

    Cette table contient les modèles compatibles


| Attribut    | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:-------:|
| model_A     | int         |             | ✓           |        |
| model_B     | int         |             | ✓           |        |

### Table `campaigns_pieces`

    Cette table contient les audits


| Attribut       | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:-------:|
| id             | int         | ✓           |             |        |
| piece_id       | int         |             | ✓           |        |
| campaign_id    | int         |             | ✓           |        |
| Question_1     | varchar(255)|             |             |        |
| Question_2     | varchar(255)|             |             |        |
| Question_3     | varchar(255)|             |             |        |
| Question_4     | varchar(255)|             |             |        |
| Question_5     | varchar(255)|             |             |        |


### Table `roles_users`

    Cette table contient les rôles associés au utilisateurs


| Attribut       | Type        | Primary Key | Foreign Key | Unique |
|----------------|-------------|:-----------:|:-----------:|:------:|
| role_id        | int         |             |    ✓        |        |
| user_id        | int         |             |     ✓       |        |

### Table `modeltemp`

    Cette table contient les modèles de pièces et de matériels temporairement dans le cadre de l'envoi de CSV   
    quand un utilisateur comfirme l'insertion des pièces ou matériels alors les éléments de cette tables sont   
    supprimer.


| Attribut        | Type        | Primary Key | Foreign Key | Unique |
|----------------|------------- |:-----------:|:-----------:|:-------:|
| id              | int         | ✓           |             |        |
| name            | varchar(255)|             |             | ✓      |
| creation_year   | int         |             |             |        |
| status          | varchar(50) |             |             |        |
| has_electro     | int         |             |             |        |
| constructor_id  | int         |             | ✓           |        |
| type_id         | int         |             | ✓           |        |
| compose         | text        |             |             |        |

![MPD inspector](/Basededonnées/mpd/mpd.png)