<?php

use App\Http\Controllers\CampaignapiController;
use App\Http\Controllers\MaterialapiController;
use App\Http\Controllers\PieceapiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;
use Laravel\Sanctum\Sanctum;

// Cette route récupère les données de l'utilisateur 
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Cette route sert pour l'authentification de l'utilisateur
Route::post('/tokens/create', function (Request $request) {
    $credentials = $request->only('name', 'password');

    if (Auth::attempt($credentials)) {
        $token = $request->user()->createToken('Token Name')->plainTextToken;

        return response()->json([
            'user' => $request->user()->name,
            'access_token' => $token,
            'token_type' => 'Bearer',
        ]);
    }else{

        return response()->json(['message' => 'Nom ou mot de passe invalide !'], 401);
    }

})->name('login.api');

// Les routes se trouvant à l'intérieur sont protégées par le middleware sanctum
Route::middleware(['auth:sanctum'])->group(function () {
    Route::apiResources([
        'material' => MaterialapiController::class,
        'piece' => PieceapiController::class,
        'campaign' => CampaignapiController::class,
    ]);
});



