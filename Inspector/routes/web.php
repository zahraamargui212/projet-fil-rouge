<?php

use App\Http\Controllers\MaterialController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PieceController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CampaignController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Auth/Login', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    
    Route::resource('/campaign', CampaignController::class);
    Route::resource('piece', PieceController::class);
    Route::resource('user', UserController::class);
    Route::put('/campaign/{campaign}/updateStatus', [CampaignController::class, 'updateStatus'])->name('status.update');
});


Route::post('/importcsv', [PieceController::class, 'importcsv'])->name('importcsv');
Route::post('/confirmpiece', [PieceController::class, 'confirmPiece'])->name('ecrasePiece');


Route::post('/userimportcsv', [UserController::class, 'importcsv'])->name('user.importcsv');

Route::resource('material', MaterialController::class);
Route::post('/confirmmaterial', [MaterialController::class, 'confirmMaterial'])->name('ecraseMaterial');
Route::post('/material/importcsv', [MaterialController::class, 'importcsv'])->name('material.importcsv');

Route::post('/saveaudit/{campaign}',[CampaignController::class, 'synchronizeQuestions'])->name('audit.save');




require __DIR__.'/auth.php';
