describe('inscription', () => {
    it('passes', () => {
      cy.visit('http://127.0.0.1:8000')
      cy.get('a').contains('Register').click()
      cy.get('input[type=text]').click().type('lala')
      cy.get('input[type=email]').click().type('lala@lala.fr')
      cy.get('input[id=password]').click().type('lalalal')
      cy.get('input[id=password_confirmation]').click().type('lalalal')
      cy.get('button').click()
    })
  })