<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Campaign;
use App\Models\Constructor;
use App\Models\modele;
use App\Models\Piece;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $reponse = ['Oui', 'Non'];

        $roles = [
            ['role' => 'Auditeur'],
            ['role' => 'Chef de Projet'],
            ['role' => 'Chargé de Referentiel'],
            ['role' => 'responsable sécurité']
        ];

        \App\Models\User::factory(2)->create();
        \App\Models\Type::factory(23)->create();
        \App\Models\Site::factory(5)->create();

        $constructor = [
            ['id' => 123, 'name' => 'MZR'],
            ['id' => 754, 'name' => 'HPM'],
            ['id' => 342, 'name' => 'RTE'],
            ['id' => 983, 'name' => 'OLI'],
            ['id' => 509, 'name' => 'PIR']
        ];
        foreach ($roles as $r) {
            $eachroles = new Role($r);
            $eachroles->save();
        }
        foreach ($constructor as $c) {
            $eachconstructor = new Constructor($c);
            $eachconstructor->save();
        }
        $iduser = User::pluck('id')->toArray();
        $idrole = Role::pluck('id')->toArray();

        for ($i = 0; $i < 2; $i++) {
            DB::table('roles_users')->insert([
                'user_id' => Arr::random($iduser),
                'role_id' => Arr::random($idrole),
            ]);
        };

        \App\Models\Modele::factory(7)->create();

        $idmodele = Modele::pluck('id')->toArray();

        for ($i = 0; $i < 5; $i++) {
            $modelA = Arr::random($idmodele);
            $modelB = Arr::random($idmodele);

            // Vérifier si les ID sont différents
            while ($modelA === $modelB) {
                $modelB = Arr::random($idmodele);
            }

            DB::table('compatible_models')->insert([
                'model_A' => $modelA,
                'model_B' => $modelB,
            ]);
        }

        $modelpiece = Arr::random($idmodele);
        $idmodel = Arr::random($idmodele);


        for ($i = 0; $i < 4; $i++) {

            $modelpiece = Arr::random($idmodele);
            $idmodel = Arr::random($idmodele);

            DB::table('models')->where('id', $idmodel)->update([

                'compose' => $modelpiece,
            ]);
        }
        \App\Models\Campaign::factory(5)->create();
        \App\Models\Material::factory(10)->create();
        \App\Models\Piece::factory(10)->create();


        $piece = Piece::pluck('id')->toArray();
        $campaign = Campaign::pluck('id')->toArray();

        for ($i = 0; $i < 5; $i++) {
            DB::table('campaigns_pieces')->insert([
                'piece_id' => Arr::random($piece),
                'campaign_id' => Arr::random($campaign),
                'Question_1' => Arr::random($reponse),
                'Question_2' => Arr::random($reponse),
                'Question_3' => Arr::random($reponse),
                'Question_4' => Arr::random($reponse),
                'Question_5' => Arr::random($reponse),
                'Question_6' => Arr::random($reponse),
            ]);
        };

        $idmodeleMaterial = Modele::pluck('id')->whereNull('compose')->toArray();

        DB::table('materials')->insert([
            [
                'id' => 'MDLK4',
                'model_id' => Arr::random($idmodeleMaterial),
                'site_id' => 2,
                'constructor_id' => 123,
                'description' => 'valeur_description_1'
            ],
        ]);

        $idmodelePiece = Modele::whereNotNull('compose')->pluck('id')->toArray();

        DB::table('pieces')->insert([
            [
                'date_mer' => 2017,
                'status_fonctionnel' => '1',
                'material_id' => 'MDLK4',
                'model_id' => Arr::random($idmodelePiece),
            ],
            [
                'date_mer' => 2022,
                'status_fonctionnel' => '0',
                'material_id' => 'MDLK4',
                'model_id' => Arr::random($idmodelePiece),

            ],
            [
                'date_mer' => 2016,
                'status_fonctionnel' => '1',
                'material_id' => 'MDLK4',
                'model_id' => Arr::random($idmodelePiece),

            ],
        ]);

        $user = new User();
        $user->name = "admin";
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->save();

        $admin = $user->where('name', 'admin')->get('id');
        $id = $user->id;

        for ($i = 1; $i < 5; $i++) {
            DB::table('roles_users')->insert([
                'user_id' => $id,
                'role_id' => $i,
            ]);
        }
    }


    // \App\Models\User::factory()->create([
    //     'name' => 'Test User',
    //     'email' => 'test@example.com',
    // ]);

}
