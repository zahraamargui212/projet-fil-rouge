<?php

namespace Database\Factories;

use App\Models\Constructor;
use App\Models\Modele;
use App\Models\Type;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ModeleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $constr = Constructor::pluck('id')->toArray();
        $type = Type::pluck('id')->toArray();


        return [
            //
           'id' => fake()->unique()->citySuffix(),
            'name' => fake()->word(),
            'creation_year' => fake()->year(),
            'has_electro' => \fake()->boolean(),
            'status' => fake()->word(),
            'constructor_id' => fake()->randomElement($constr),
            'type_id' => fake()->randomElement($type),

        ];
    }
}
