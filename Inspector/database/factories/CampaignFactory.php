<?php

namespace Database\Factories;

use App\Models\Site;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CampaignFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $iduser = User::pluck('id')->toArray();
        $idsite = Site::pluck('id')->toArray();
        $status = ['Prête','En cours','Terminée','Terminée et synchronisée'];
        return [
            //
            'date_start'=> fake()->date(),
            'date_end'=> fake()->date(),
            'name'=> fake()->name(),
            'status'=> fake()->randomElement($status),
            'auditor_id' => \fake()->randomElement($iduser),
            'creator_id' => \fake()->randomElement($iduser),
            'site_id' => \fake()->randomElement($idsite),


        ];
    }
}
