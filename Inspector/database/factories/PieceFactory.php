<?php

namespace Database\Factories;

use App\Models\Material;
use App\Models\Modele;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PieceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $material = Material::pluck('id')->toArray();
        $model = Modele::pluck('id')->toArray();

        return [
            //
            'status_fonctionnel'=> fake()->word('Active','Inactive'),
            'date_mer'=> fake()->year(),
            'material_id' => fake()->randomElement($material),
            'model_id' => fake()->randomElement($model),
        ];
    }
}
