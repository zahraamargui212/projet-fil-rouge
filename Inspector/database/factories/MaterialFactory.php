<?php

namespace Database\Factories;

use App\Models\Constructor;
use App\Models\Modele;
use App\Models\Site;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class MaterialFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $site = Site::pluck('id')->toArray();
        $constructor = Constructor::pluck('id')->toArray();
        $model = Modele::pluck('id')->toArray();

        return [
            //
            'id' => fake()->word(),
            'model_id' => \fake()->randomElement($model),
            'site_id' => \fake()->randomElement($site),
            'constructor_id' => \fake()->randomElement($constructor),
            'description' => fake()->sentence(),

        ];
    }
}
