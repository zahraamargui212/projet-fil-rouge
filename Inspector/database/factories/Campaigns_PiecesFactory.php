<?php

namespace Database\Factories;

use App\Models\Campaign;
use App\Models\Piece;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class Campaigns_PiecesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $piece = Piece::pluck('id')->toArray();
        $campaign = Campaign::pluck('id')->toArray();
        return [
            'piece_id'=> fake()->randomElement($piece),
            'campaign_id'=> fake()->randomElement($campaign),
            'Question_1' => fake()->sentence(),
            'Question_2' => fake()->sentence(),
            'Question_3' => fake()->sentence(),
            'Question_4' => fake()->sentence(),
            'Question_5' => fake()->sentence(),
            'Question_6' => fake()->sentence(),
        ];
    }
}
