<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();
            $table->date('date_start');
            $table->date('date_end');
            $table->string('name');
            $table->string('status');
            $table->foreignId('auditor_id')->nullable();
            $table->foreign('auditor_id')->references('id')->on('users');
            $table->foreignid('creator_id');
            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreignId('site_id');
            $table->foreign('site_id')->references('id')->on('sites');
            
        });
    }



    
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
