<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('pieces', function (Blueprint $table) {
            $table->id();
            $table->integer('date_mer');
            $table->string('status_fonctionnel');
            $table->string('material_id')->nullable()->constrained();
            $table->string('model_id')->constrained();
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
