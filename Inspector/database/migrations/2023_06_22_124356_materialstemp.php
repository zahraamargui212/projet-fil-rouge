<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('materialtemp', function (Blueprint $table) {
            $table->string('id');
            $table->string('nom');
            $table->foreignId('site_id')->constrained();
            $table->foreignId('constructor_id')->constrained();
            $table->date('date_mer');
            $table->integer('has_electro');
            $table->string('pieces_associes');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
