<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('modelstemp', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->string('name');
            $table->integer('creation_year');
            $table->string('status');
            $table->integer('has_electro');
            $table->string('piece_associes')->nullable();
            $table->foreignId('constructor_id')->constrained();
            $table->foreignId('type_id')->constrained();
            $table->string('compose')->references('id')->on('models')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
