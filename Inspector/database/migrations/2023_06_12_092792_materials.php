<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('materials', function (Blueprint $table) {
            $table->string('id');
            $table->string('model_id')->references('id')->on('models');
            $table->foreignId('site_id')->constrained();
            $table->foreignId('constructor_id')->constrained();
            $table->text('description');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
