<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        //
        Schema::create('campaigns_pieces', function (Blueprint $table) {
            $table->id();
            $table->foreignId('piece_id')->constrained();
            $table->foreignId('campaign_id')->constrained();
            $table->string('Question_1');
            $table->string('Question_2');
            $table->string('Question_3');
            $table->string('Question_4');
            $table->string('Question_5');
            $table->string('Question_6')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
