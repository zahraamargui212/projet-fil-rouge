CREATE TABLE IF NOT EXISTS `users` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `remember_token` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

CREATE TABLE IF NOT EXISTS `password_reset_tokens` (
  `email` VARCHAR(255) NOT NULL,
  `token` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `password_reset_tokens` ADD PRIMARY KEY (`email`);

CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uuid` VARCHAR(255) NOT NULL,
  `connection` TEXT NOT NULL,
  `queue` TEXT NOT NULL,
  `payload` LONGTEXT NOT NULL,
  `exception` LONGTEXT NOT NULL,
  `failed_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `failed_jobs` ADD UNIQUE `failed_jobs_uuid_unique`(`uuid`);

CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `tokenable_type` VARCHAR(255) NOT NULL,
  `tokenable_id` BIGINT UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `token` VARCHAR(64) NOT NULL,
  `abilities` TEXT NULL,
  `last_used_at` TIMESTAMP NULL,
  `expires_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `personal_access_tokens` ADD INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`);

ALTER TABLE `personal_access_tokens` ADD UNIQUE `personal_access_tokens_token_unique`(`token`);

CREATE TABLE IF NOT EXISTS `roles` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `role` VARCHAR(255) NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `roles` ADD UNIQUE `roles_role_unique`(`role`);

CREATE TABLE IF NOT EXISTS `sites` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(255) NOT NULL,
  `adress` VARCHAR(255) NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

CREATE TABLE IF NOT EXISTS `roles_users` (
  `role_id` BIGINT UNSIGNED NOT NULL,
  `user_id` BIGINT UNSIGNED NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `roles_users` ADD CONSTRAINT `roles_users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `roles_users` ADD CONSTRAINT `roles_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `campaigns` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `date_start` DATE NOT NULL,
  `date_end` DATE NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `status` VARCHAR(255) NOT NULL,
  `auditor_id` BIGINT UNSIGNED NULL,
  `creator_id` BIGINT UNSIGNED NOT NULL,
  `site_id` BIGINT UNSIGNED NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `campaigns` ADD CONSTRAINT `campaigns_auditor_id_foreign` FOREIGN KEY (`auditor_id`) REFERENCES `users` (`id`);
ALTER TABLE `campaigns` ADD CONSTRAINT `campaigns_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`);
ALTER TABLE `campaigns` ADD CONSTRAINT `campaigns_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`);

CREATE TABLE IF NOT EXISTS `types` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom` VARCHAR(255) NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

CREATE TABLE IF NOT EXISTS `constructors` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(255) NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

CREATE TABLE IF NOT EXISTS `models` (
  `id` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `creation_year` INT NOT NULL,
  `status` VARCHAR(255) NOT NULL,
  `has_electro` INT NOT NULL,
  `constructor_id` BIGINT UNSIGNED NOT NULL,
  `type_id` BIGINT UNSIGNED NOT NULL,
  `compose` VARCHAR(255) NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `models` ADD CONSTRAINT `models_constructor_id_foreign` FOREIGN KEY (`constructor_id`) REFERENCES `constructors` (`id`);
ALTER TABLE `models` ADD CONSTRAINT `models_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`);
ALTER TABLE `models` ADD UNIQUE `models_id_unique`(`id`);

CREATE TABLE IF NOT EXISTS `pieces` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `date_mer` INT NOT NULL,
  `status_fonctionnel` VARCHAR(255) NOT NULL,
  `material_id` VARCHAR(255) NULL,
  `model_id` VARCHAR(255) NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

CREATE TABLE IF NOT EXISTS `materials` (
  `id` VARCHAR(255) NOT NULL,
  `model_id` VARCHAR(255) NOT NULL,
  `site_id` BIGINT UNSIGNED NOT NULL,
  `constructor_id` BIGINT UNSIGNED NOT NULL,
  `description` TEXT NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `materials` ADD CONSTRAINT `materials_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`);
ALTER TABLE `materials` ADD CONSTRAINT `materials_constructor_id_foreign` FOREIGN KEY (`constructor_id`) REFERENCES `constructors` (`id`);

CREATE TABLE IF NOT EXISTS `compatible_models` (
  `model_A` VARCHAR(255) NOT NULL,
  `model_B` VARCHAR(255) NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

CREATE TABLE IF NOT EXISTS `modelstemp` (
  `id` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `creation_year` INT NOT NULL,
  `status` VARCHAR(255) NOT NULL,
  `has_electro` INT NOT NULL,
  `piece_associes` VARCHAR(255) NULL,
  `constructor_id` BIGINT UNSIGNED NOT NULL,
  `type_id` BIGINT UNSIGNED NOT NULL,
  `compose` VARCHAR(255) NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `modelstemp` ADD CONSTRAINT `modelstemp_constructor_id_foreign` FOREIGN KEY (`constructor_id`) REFERENCES `constructors` (`id`);
ALTER TABLE `modelstemp` ADD CONSTRAINT `modelstemp_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`);
ALTER TABLE `modelstemp` ADD UNIQUE `modelstemp_id_unique`(`id`);

CREATE TABLE IF NOT EXISTS `materialtemp` (
  `id` VARCHAR(255) NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  `site_id` BIGINT UNSIGNED NOT NULL,
  `constructor_id` BIGINT UNSIGNED NOT NULL,
  `date_mer` DATE NOT NULL,
  `has_electro` INT NOT NULL,
  `pieces_associes` VARCHAR(255) NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `materialtemp` ADD CONSTRAINT `materialtemp_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`);
ALTER TABLE `materialtemp` ADD CONSTRAINT `materialtemp_constructor_id_foreign` FOREIGN KEY (`constructor_id`) REFERENCES `constructors` (`id`);

CREATE TABLE IF NOT EXISTS `campaigns_pieces` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `piece_id` BIGINT UNSIGNED NOT NULL,
  `campaign_id` BIGINT UNSIGNED NOT NULL,
  `Question_1` VARCHAR(255) NOT NULL,
  `Question_2` VARCHAR(255) NOT NULL,
  `Question_3` VARCHAR(255) NOT NULL,
  `Question_4` VARCHAR(255) NOT NULL,
  `Question_5` VARCHAR(255) NOT NULL,
  `Question_6` VARCHAR(255) NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE 'utf8mb4_unicode_ci';

ALTER TABLE `campaigns_pieces` ADD CONSTRAINT `campaigns_pieces_piece_id_foreign` FOREIGN KEY (`piece_id`) REFERENCES `pieces` (`id`);
ALTER TABLE `campaigns_pieces` ADD CONSTRAINT `campaigns_pieces_campaign_id_foreign` FOREIGN KEY (`campaign_id`) REFERENCES `campaigns` (`id`);
