<?php

use GuzzleHttp\Psr7\Response;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public function test_importcsv(): void
    {
        // Créer un fichier CSV de test
        $file = 'test.csv';

        // Créer une instance simulée du client HTTP
        $client = Mockery::mock(Client::class);

        // Simuler l'appel POST
        $response = new Response(302, ['Location' => '/piece.index'], null);
        $client->shouldReceive('request')
            ->once()
            ->with('POST', '/importcsv')
            ->andReturn($response);

        $this->assertEquals($response, $client->request('POST', '/importcsv'));

        // Vérifier les modifications dans la base de données, si nécessaire
    }
}
