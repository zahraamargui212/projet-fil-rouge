create database IF NOT EXISTS inspector;

CREATE USER
    IF NOT EXISTS 'inspector' @'localhost' IDENTIFIED BY 'pass_inspector';

GRANT
    ALL PRIVILEGES ON inspector.* TO 'inspector' @'localhost';

FLUSH PRIVILEGES;

use inspector;
