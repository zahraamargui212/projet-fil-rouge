<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Material extends Model
{
    use HasFactory;
    public $incrementing = false;
    public $timestamps = false;
    protected $keyType = 'string';

    protected $fillable = [
        'id',
        'nom',
        'site_id',
        'constructor_id',
        'date_mer',
        'has_electro'

    ];

    /**
     * Relationship with sites table
     *
     * @return BelongsTo
     */
    public function site():BelongsTo
    {
        return $this->belongsTo(Site::class,'site_id');
    }

    /**
     * Relationship with pieces table
     *
     * @return HasMany
     */
    public function piece(): HasMany
    {
        return $this->hasMany(Piece::class);
    }

    /**
     * Relationship with models table
     *
     * @return BelongsTo
     */
    public function modele(): BelongsTo
    {
        return $this->belongsTo(Modele::class, 'model_id');
    }

}
