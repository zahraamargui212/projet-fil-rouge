<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Piece extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'id',
        'nom',
        'status_fonctionnel',
        'has_electro',
        'creation_year',
        'material_id',
        'model_id'

    ];



    /**
     * Relationship with material
     *
     * @return BelongsTo
     */
    public function material(): BelongsTo
    {
        return $this->belongsTo(Material::class, 'material_id');
    }

    /**
     * Relationship with model
     *
     * @return BelongsTo
     */
    public function model(): BelongsTo
    {
        return $this->belongsTo(Modele::class, 'model_id');
    }

    /**
     * Relationship with campaign
     *
     * @return BelongsToMany
     */
    public function campaign(): BelongsToMany
    {
        return $this->belongsToMany(Campaign::class, 'campaigns_pieces','piece_id','campaign_id')
        ->withPivot('Question_1','Question_2','Question_3','Question_4','Question_5','Question_6',);
    }

}
