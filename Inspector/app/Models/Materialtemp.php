<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materialtemp extends Model
{
    use HasFactory;
    public $table = 'materialtemp';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;


}
