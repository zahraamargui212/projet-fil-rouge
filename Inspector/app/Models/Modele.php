<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Modele extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'date_mm',
        'status',
        'constructor_id',
        'type_id'
    ];

    protected $table ='models';
    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;

    /**
     * Relationship with pieces table
     *
     * @return HasMany
     */
    public function piece(): HasMany
    {
        return $this->hasMany(Piece::class);
    }

    /**
     * Relationship with constructors table
     *
     * @return BelongsTo
     */
    public function constructor(): BelongsTo
    {
        return $this->belongsTo(Constructor::class, 'constructor_id');
    }

    /**
     * Relationship with types table
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class, 'type_id');
    }

    /**
     * Relationship with models table : first model
     *
     * @return BelongsToMany
     */
    public function modelA(): BelongsToMany
    {
        return $this->belongsToMany(Modele::class, 'compatible_models', 'model_A', 'model_B');
    }

    /**
     * Relationship with models table (pivot table) : second model
     *
     * @return BelongsToMany
     */
    public function modelB(): BelongsToMany
    {
        return $this->belongsToMany(Modele::class, 'compatible_models', 'model_B', 'model_A');
    }

    /**
     * Relationship with material table
     *
     * @return hasMany
     */
    public function material(): hasMany
    {
        return $this->hasMany(Material::class);
    }

    /**
     * Relationship with model : pieces models that belongs to material model
     *
     * @return BelongsTo
     */
    public function pieceMaterial(): BelongsTo{

        return $this->belongsTo(Modele::class, 'compose');

    }

    /**
     * Relationship with model : material that has many pieces
     *
     * @return HasMany
     */
    public function pieceMaterialinverse(): HasMany{

        return $this->hasMany(Modele::class,'compose');

    }
    
}
