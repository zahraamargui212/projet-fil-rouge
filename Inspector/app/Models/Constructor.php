<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Constructor extends Model
{
    use HasFactory;

    protected $table = 'constructors';

    public $timestamps = false;
    public $incrementing = false;

       /**
        * Relationship with models table
        *
        * @return HasMany
        */
       public function model():HasMany
       {
           return $this->hasMany(Model::class);
       }
}
