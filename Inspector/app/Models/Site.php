<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Site extends Model
{
    use HasFactory;
    public $timestamps = false;

    /**
     * Relationship with campaign
     *
     * @return HasMany
     */
    public function campaign():HasMany
    {
        return $this->hasMany(Campaign::class);
    }

    /**
     * Relationship with material
     *
     * @return HasMany
     */
    public function material():HasMany
    {
        return $this->hasMany(Material::class);
    }
}
