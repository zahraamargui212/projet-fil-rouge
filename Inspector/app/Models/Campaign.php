<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


class Campaign extends Model
{
    use HasFactory;
    
    public $timestamps = false;
    
    /**
     * Relationship with sites
     *
     * @return BelongsTo
     */
    public function sites (): BelongsTo
    {
        return $this->belongsTo(Site::class,'site_id');
    }

    
    /**
     * Relationship with users table for the creator of the campaign
     *
     * @return BelongsTo
     */
    public function creator(): BelongsTo {
        return $this->belongsTo(User::class,'creator_id');
    }

    /**
     * Relationship with users table for the auditor of the campaign
     *
     * @return BelongsTo
     */
    public function auditor(): BelongsTo {
        return $this->belongsTo(User::class,'auditor_id');
    }

    /**
     * Relationship with pieces table
     *
     * @return BelongsToMany
     */
    public function piece(): BelongsToMany
    {
        return $this->belongsToMany(Piece::class, 'campaigns_pieces','campaign_id','piece_id')
        ->withPivot('Question_1','Question_2','Question_3','Question_4','Question_5','Question_6');
    }

}

