<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public $timestamps = false;
    
    /**
     * Relationship with roles
     *
     * @return void
     */
    public function users(){
        return $this->belongsToMany(User::class,'roles_users','role_id','user_id');
    }
}
