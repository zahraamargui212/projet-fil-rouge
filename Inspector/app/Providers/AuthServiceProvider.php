<?php

namespace App\Providers;

use App\Models\Piece;
use App\Models\User;
use App\Policies\UserPolicy;
use App\Models\Campaign;
use App\Models\Material;
use App\Policies\CampaignPolicy;
use App\Policies\MaterialPolicy;
use App\Policies\PiecePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [

        User::class => UserPolicy::class,
        Piece::class => PiecePolicy::class,
        Material::class => MaterialPolicy::class,
        Campaign::class=>CampaignPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //

    }
}
