<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CampaignapiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // récupère les campagne avec les infos de l'auditeur et du créateur de campagne
        $campaigns = Campaign::with(['creator', 'auditor','sites.material.piece'])->get();

        return response()->json($campaigns);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Campaign $campaign)
    {
        $pieces = Campaign::where('id', $campaign->id)->with(['creator', 'auditor','sites.material.piece'])->get();

        return response()->json($pieces);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
