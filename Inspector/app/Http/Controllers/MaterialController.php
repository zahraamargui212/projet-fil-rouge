<?php

namespace App\Http\Controllers;

use App\Models\Constructor;
use App\Models\Material;
use App\Models\Materialtemp;
use App\Models\Modele;
use App\Models\Modeltemp;
use App\Models\Piece;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->authorize('viewAny', Material::class);

        // récupère le rôle de l'utilisateur connecté
        $user = Auth::user();
        $role = $user->roles;

        $material = Modele::with('constructor', 'pieceMaterialinverse')
            ->whereNull('compose')
            ->get();

        $existing = Modeltemp::get();
        return Inertia::render('Material/Index', ['materials' => $material, 'existing' => $existing, 'role' => $role]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Material $material)
    {

        $material = $material->load(['site', 'piece']);
        return Inertia::render('Material/Show', ['material' => $material]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }



    /**
     * Import csv 
     *
     * @return void
     */
    public function importcsv(Request $materialrequest)
    {

        // 'import' correspond à la  function import de MaterialPolicy
        $this->authorize('import', Material::class);
        if ($materialrequest['csv'] !== null) {

            // Placer le fichier CSV dans le dossier public/csv
            if ($materialrequest->hasFile('csv')) {
                $path = '/csv';
                $mycsv = $materialrequest->file('csv')->getClientOriginalName();
                $materialrequest->file('csv')->move(public_path($path), $mycsv);
            }

            $csvFile = fopen(public_path("csv" . "/" . $mycsv), "r");
            $firstline = true;
            $modelsData = []; // Tableau pour stocker les données des modèles
            $duplicateFound = false;

            try {
                while (($data = fgetcsv($csvFile, 10240, ",")) !== false) {
                    if (!$firstline) {
                        /** Détails des colonnes du CSV Matérial
                         * 0 constructor_id
                         * 1 id_materiel	
                         * 2 nom_materiel	
                         * 3 date_mer	
                         * 4 Has_electro	
                         * 5 site_id	
                         * 6 id_pieces
                         * 7 status
                         * 8 type_id
                         */

                        $existingModel = Modele::where('name', $data[2])->first();

                        if ($existingModel) {
                            $duplicateFound = true;
                        }

                        $modelData = [
                            'id' => $data[1],
                            'name' => $data[2],
                            'creation_year' => $data[3] ?? null, // Utilisation de null si la valeur est absente dans le CSV
                            'status' => $data[7],
                            'has_electro' => $data[4],
                            'constructor_id' => $data[0],
                            'type_id' => $data[8],
                            'piece_associes' => $data[6],
                        ];

                        $modelsData[] = $modelData;
                    }

                    $firstline = false;
                }

                fclose($csvFile);

                if ($duplicateFound) {
                    foreach ($modelsData as $modelData) {
                        $modele = new Modeltemp();
                        $modele->id = $modelData['id'];
                        $modele->name = $modelData['name'];
                        $modele->creation_year = $modelData['creation_year'];
                        $modele->status = $modelData['status'];
                        $modele->has_electro = $modelData['has_electro'];
                        $modele->constructor_id = $modelData['constructor_id'];
                        $modele->type_id = $modelData['type_id'];
                        $modele->piece_associes = $modelData['piece_associes'];
                        $modele->save();
                    }
                } else {
                    foreach ($modelsData as $modelData) {
                        $modele = new Modele();
                        $modele->id = $modelData['id'];
                        $modele->name = $modelData['name'];
                        $modele->creation_year = $modelData['creation_year'];
                        $modele->status = $modelData['status'];
                        $modele->has_electro = $modelData['has_electro'];
                        $modele->constructor_id = $modelData['constructor_id'];
                        $modele->type_id = $modelData['type_id'];
                        $modele->save();

                        // Envoi des données dans la table pieces
                        $idPiece = $modelData['piece_associes'];
                        $data_pieces = explode('|', $idPiece);

                        $piecesToSave = Modele::whereIn('id', $data_pieces)->get();
                        foreach ($piecesToSave as $piece) {
                            $piece->compose = $modelData['id'];
                            $piece->save();
                        }
                    }
                }

                return redirect()->route('material.index')->with('success', 'Les nouvelles données ont bien été insérées avec succès');
                
            } catch (\Throwable $th) {
                return redirect()->route('material.index')->with('error', 'Les données n\'ont pas été envoyées !');
            }
        } else {
            return redirect()->route('material.index')->with('error', 'Vous devez séléctionner un fichier CSV');
        }
    }





    /**
     * Comfirm piece
     *
     * @return void
     */
    public function confirmMaterial(Request $request)
    {
        $jsonData = $request['existingData'];
        $question = $request['overwriteMaterials'];


        if ($question == 'yes') {

            foreach ($jsonData as $data) {
                //  \dd($data);


                // Insérer les données dans la table "materials"
                $piece = Modele::updateOrCreate(['id' => $data['id']]);
                $piece->name = $data['name'];
                $piece->creation_year = $data['creation_year'];
                $piece->has_electro = $data['has_electro'];
                $piece->status = $data['status'];
                $piece->constructor_id = $data['constructor_id'];
                $piece->type_id = $data['type_id'];
                $piece->save();

                // \dd($piece);

                // Envoi des données dans la table pieces
                $idPiece = $data['piece_associes'];
                $data_pieces = explode('|', $idPiece);


                $piecesToSave = Modele::whereIn('id', $data_pieces)->get();
                foreach ($piecesToSave as $piece) {
                    $piece->compose = $data['id'];
                    $piece->save();
                }
            }

            Modeltemp::truncate();

            return redirect()->route('material.index')->with('success', 'Les données ont bien étaient mise à jour');
        }
        if ($question == 'no') {

            Modeltemp::truncate();
        }
    }
}

//         $csvFile = fopen(base_path("public/csv" . "/" . $mycsv), "r");
//         $firstline = true;
    
//         $existingMaterials = [];
//         $pieces = [];
    
//         while (($data = fgetcsv($csvFile, 10240, ",")) !== FALSE) {
//             if (!$firstline) {
    
//                 /**
//                  * 0 constructor_id
//                  * 1 id_materiel	
//                  * 2 nom_materiel	
//                  * 3 date_mer	
//                  * 4 Has_electro	
//                  * 5 site_id	
//                  * 6 id_pieces
//                  */
    
//                 // Envoi des données dans la table materials
//                 $material = Material::firstOrNew(['id' => $data[1]]);
//                 $material->nom = $data[2];
//                 $material->date_mer = $data[3];
//                 $material->site_id = $data[5];
//                 $material->constructor_id = $data[0];
//                 $material->save();
    
//                 // Envoi des données dans la table pieces
//                 $idPiece = $data[6];
//                 $data_pieces = explode('|', $idPiece);
    
//                 $pieces[] = $data_pieces;
    
//                 $piecesToSave = Piece::whereIn('id', $data_pieces)->get();
//                 foreach ($piecesToSave as $piece) {
//                     $piece->material_id = $data[1];
//                     $piece->save();
//                 }
    
//                 if (!$material->wasRecentlyCreated) {
//                     // La material était déjà présente en base, on l'ajoute au tableau des pièces existantes
//                     $existingMaterials[] = $material;
//                 }
//             }
    
//             $firstline = false;
//         }
    
//         fclose($csvFile);
    
//         $materials = Material::with(['site'])->get();
//         return Inertia::render('Material/Index', ['existing' => $existingMaterials, 'materials' => $materials, 'pieces' => $pieces]);
//     }
    

    
//     /**
//      * Comfirm piece
//      *
//      * @return void
//      */
//     public function confirmMaterial(Request $request)
//     {
//         $jsonData = $request->json()->all();
//         $question = $request['overwriteMaterials'];

//         if ($question == 'yes') {
//             foreach ($jsonData as $data) {


//                 // Insérer les données dans la table "materials"
//                 $material = Material::firstOrNew(['nom' => $data['nom']]);
//                 $material->constructor_id = $data['constructor_id'];
//                 $material->id_material = $data['material_id'];
//                 $material->date_mer = $data['date_mer'];
//                 $material->site_id = $data['site_id'];
//                 $material->save();
//             }

//             return redirect()->route('material.index')->with('success', 'Données insérées avec succès');
//         }
//     }
// }
