<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Policies\UserPolicy;
use Illuminate\Http\Request;
use Inertia\Inertia;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $this->authorize('viewAny', User::class);

        $user = User::with(['roles'])->get();
        return Inertia::render('User/Index', ['users' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
        $user = $user->load(['roles']);
        return Inertia::render('role/Show', ['role' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    /**
     * Import csv 
     *
     * @return void
     */
    public function importcsv(Request $userrequest)
    {

        if ($userrequest['csv'] !== null ) {


            $this->authorize('importCsv', User::class);

            // placer le fichier CSV dans le dossier public/csv

            try {

                if ($userrequest->hasFile('csv')) {
                    $path = '/csv';
                    $mycsv = $userrequest->file('csv')->getClientOriginalName();
                    $userrequest->file('csv')->move(public_path($path), $mycsv);
                }
                /**
                 * 0 name
                 * 1 password
                 * 2 role
                 */

                $csvFile = fopen(base_path("public/csv" . "/" . $mycsv), "r");
                $firstline = true;


                // while (($data = fgetcsv($csvFile, 10240, ",")) !== FALSE) {

                //     if (!$firstline) {

                //         // Chercher l'utilisateur, sinon en créer un nouveau
                //         $user = User::firstOrCreate(
                //             ['name' => $data[0]], // les conditions de recherche
                //             ['password' => $data[1]] // les valeurs à insérer si l'utilisateur n'existe pas
                //         );

                //         // Maintenant, attachez les rôles à l'utilisateur
                //         $roleNames = explode('|', $data[2]);
                //         $roles = Role::whereIn('id', $roleNames)->get();


                //         // $user->roles()->sync($roles->pluck('id'));
                //         $user->roles()->syncWithoutDetaching($roles->pluck('id'));



                //     }
                while (($data = fgetcsv($csvFile, 10240, ",")) !== FALSE) {

                    if (!$firstline) {

                        // Chercher l'utilisateur. 
                        $user = User::firstWhere('name', $data[0]);

                        // si l'utilisateur n'existe pas, en créer un nouveau et ajouter les rôles
                        if (!$user) {
                            $user = User::create([
                                'name' => $data[0],
                                'password' => $data[1],
                            ]);

                            $roleNames = explode('|', $data[2]);
                            $roles = Role::whereIn('id', $roleNames)->get();

                            $user->roles()->sync($roles->pluck('id'));
                        }
                    }

                    $firstline = false;
                }

                fclose($csvFile);

                return redirect()->route('user.index')->with('success', 'Les données ont été correctement insérées');
                
            } catch (\Throwable $th) {

                return redirect()->route('user.index')->with('error', 'Les données n\'ont pas été insérées');
            }

        } else {

            return redirect()->route('user.index')->with('error', 'Vous devez séléctionner un fichier CSV');
        }
    }
}
