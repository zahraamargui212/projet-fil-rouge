<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Material;
use App\Models\Piece;
use App\Models\Site;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $this->authorize('viewAny', Campaign::class);

        // récupère les campagne avec les infos de l'auditeur et du créateur de campagne
        $campaigns = Campaign::with(['creator', 'auditor'])->get();

        // récupère le rôle de l'utilisateur connecté
        $user = Auth::user();
        $role = $user->roles;

        // envoi à la vue les infos récupérer plus haut
        return Inertia::render('Campaign/Index', ['campaigns' => $campaigns, 'role' => $role]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('create', Campaign::class);

        $site = Site::with(['Material.modele'])
            ->get();



        return Inertia::render('Campaign/Create', ['sites' => $site]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->authorize('create', Campaign::class);

        $datas = $request->validate([
            'name' => ['required', 'string'],
            'date_start' => ['required', 'date'],
            'date_end' => ['required', 'date'],
            'selectedSite' => ['required', 'array'],
        ]);

        $id = User::where('name', Auth::user()->name)->first()->id;


        $c = new Campaign();
        $c->name = $datas['name'];
        $c->site_id = $datas['selectedSite']['id'];
        $c->date_start = $datas['date_start'];
        $c->date_end = $datas['date_end'];
        $c->status = 'Créer';
        $c->creator_id = $id;
      

        $c->save();

        // données récupérées du formulaire
        $material_ids = [];
        foreach ($datas['selectedSite']['material'] as $materials) {
            $material_ids[] = $materials['model_id'];
        }

        // données récupérées de la bdd

        // TODO: AMELIORER L'UPDATE MATERIAL reduire à deux requetes
        // $materials =  Material::whereIn('model_id', $material_ids)->get();
        // foreach($materials as $material) {
        //     $material->description = $datas['description'];
        // }
        // $materials->save();

        foreach ($datas['selectedSite']['material'] as $material) {
            $dbMat = Material::find($material['id']);
            $dbMat->description = $material['description'];
            $dbMat->save();

        }


        return redirect()->intended(route('campaign.create'))->with('success', 'Votre campagne a été créé avec succès');
    }

    /**
     * Display the specified resource.
     */
    public function show(Campaign $campaign)
    {
        $this->authorize('view', Campaign::class);

        $siteId = $campaign->site_id;
        $site = Site::with(['material', 'material.piece', 'material.piece.model'])->where('id', $siteId)->first();
        return Inertia::render('Campaign/Show', ['campaign' => $campaign, 'site' => $site]);
    }

    // public function updateStatus(Request $request, Campaign $campaign)
    // {
    //     $campaign->status = $request->input('status');
    //     $campaign->save();

    //     return response()->json(['message' => 'Statut de la campagne mis à jour']);
    // }

    public function updateStatus(Request $request, Campaign $campaign)
    {
        // $this->authorize('update', $campaign);

        $campaign->status = $request->input('status');
        $campaign->save();

        return redirect()->intended(route('campaign.show',$campaign))->with('success', 'Votre audit a été ' . $request['status'] .' avec succès');
    }



    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }


    public function synchronizeQuestions(Request $request, Campaign $campaign)
    {
        $pieces = json_decode($request->getContent(), true);
        $campaign->status = 'terminée et synchronisée';
        $campaign->save();

        foreach ($pieces as $pieceData) {
            $piece = Piece::find($pieceData['id']);

            $piece->campaign()->syncWithPivotValues($pieceData['audit_id'], [
                'Question_1' => $pieceData['Question_1'],
                'Question_2' => $pieceData['Question_2'],
                'Question_3' => $pieceData['Question_3'],
                'Question_4' => $pieceData['Question_4'],
                'Question_5' => $pieceData['Question_5'],
                'Question_6' => $pieceData['Question_6'] ?? null,
                
            ]);
        }

        return redirect()->intended(route('campaign.show',$campaign))->with('success', 'Votre audit a été ' . $request['status'] .' avec succès');

    }
}
