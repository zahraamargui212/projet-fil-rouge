<?php

namespace App\Http\Controllers;

use App\Models\Piece;
use Illuminate\Http\Request;
use Laravel\Sanctum\Sanctum;

class PieceapiController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth:sanctum')->only(['index','store', 'update', 'destroy']);
    // }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $piece = Piece::all();
    
        return response()->json($piece);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
