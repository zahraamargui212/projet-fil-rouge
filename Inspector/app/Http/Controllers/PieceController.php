<?php

namespace App\Http\Controllers;

use App\Models\Constructeur;
use App\Models\Material;
use App\Models\Modele;
use App\Models\Modeltemp;
use App\Models\Piecetemp;
use App\Models\Piece;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;


class PieceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->authorize('viewAny', Piece::class);
        // récupère le rôle de l'utilisateur connecté
        $user = Auth::user();
        $role = $user->roles;
        $piece = Modele::where('compose', '!=', "NULL")->get();
        $existing = Modeltemp::get();


        return Inertia::render('Piece/Index', ['pieces' => $piece, 'existing' => $existing, 'role' => $role]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $pieceequest)
    {
    }

    /**
     * Display the specified resource.
     */
    public function show(Piece $piece)
    {
        $piece = $piece->load(['model',  'material']);
        return Inertia::render('Piece/Show', ['piece' => $piece]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $pieceequest, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    /**
     * Import CSV 
     *
     * @return void
     */
    public function importcsv(Request $piecerequest)
    {
        if ($piecerequest['csv'] !== null) {

            try {

                // Placer le fichier CSV dans le dossier public/csv
                if ($piecerequest->hasFile('csv')) {
                    $path = '/csv';
                    $mycsv = $piecerequest->file('csv')->getClientOriginalName();
                    $piecerequest->file('csv')->move(public_path($path), $mycsv);
                }

                /**
                 * 0 constructor_id
                 * 1 model_id
                 * 2 model_name
                 * 3 type_id
                 * 4 creation_year
                 * 5 has_electro
                 * 6 status
                 */

                $csvFile = fopen(public_path("csv" . "/" . $mycsv), "r");
                $firstline = true;
                $modelsData = []; // Tableau pour stocker les données des modèles
                $duplicateFound = false;

                while (($data = fgetcsv($csvFile, 10240, ",")) !== FALSE) {
                    if (!$firstline) {
                        $existingModel = Modele::where('id', $data[1])->first();

                        if ($existingModel) {
                            $duplicateFound = true;
                        }

                        $modelData = [
                            'id' => $data[1],
                            'name' => $data[2],
                            'creation_year' => $data[4],
                            'status' => $data[6],
                            'has_electro' => $data[5],
                            'constructor_id' => $data[0],
                            'type_id' => $data[3],
                        ];

                        $modelsData[] = $modelData;
                    }

                    $firstline = false;
                }

                fclose($csvFile);

                if ($duplicateFound) {
                    foreach ($modelsData as $modelData) {
                        $modele = new Modeltemp();
                        $modele->id = $modelData['id'];
                        $modele->name = $modelData['name'];
                        $modele->creation_year = $modelData['creation_year'];
                        $modele->status = $modelData['status'];
                        $modele->has_electro = $modelData['has_electro'];
                        $modele->constructor_id = $modelData['constructor_id'];
                        $modele->type_id = $modelData['type_id'];
                        $modele->save();
                    }
                } else {
                    foreach ($modelsData as $modelData) {
                        $modele = new Modele();
                        $modele->id = $modelData['id'];
                        $modele->name = $modelData['name'];
                        $modele->creation_year = $modelData['creation_year'];
                        $modele->status = $modelData['status'];
                        $modele->has_electro = $modelData['has_electro'];
                        $modele->constructor_id = $modelData['constructor_id'];
                        $modele->type_id = $modelData['type_id'];
                        $modele->save();
                    }
                }

                return redirect()->route('piece.index')->with('success', 'Les données ont été insérées avec succès');
            } catch (\Throwable $th) {

                return redirect()->route('piece.index')->with('error', 'Les données n\'ont pas été insérés');
            }
        } else {
            return redirect()->route('piece.index')->with('error', 'Vous devez séléctionner un fichier CSV');
        }
    }



    /**
     * Confirm piece
     *
     * @return void
     */
    public function confirmPiece(Request $request)
    {
        $jsonData = $request['existingData'];
        $question = $request['overwritePieces'];

        if ($question == 'yes') {
            foreach ($jsonData as $data) {
                $piece = Modele::updateOrCreate(['id' => $data['id']], [
                    'name' => $data['name'],
                    'creation_year' => $data['creation_year'],
                    'has_electro' => $data['has_electro'],
                    'status' => $data['status'],
                    'constructor_id' => $data['constructor_id'],
                    'type_id' => $data['type_id']
                ]);
            }

            Modeltemp::truncate();
            return redirect()->route('piece.index')->with('success', 'Données insérées avec succès');
        }

        if ($question == 'no') {
            Modeltemp::truncate();
        }

        return redirect()->back();
    }
}
