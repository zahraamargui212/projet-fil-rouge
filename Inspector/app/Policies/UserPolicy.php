<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    /**
     * Determine whether the user can view any users.
     */
    public function viewAny(User $user): Bool
    {
        return $user->roles->contains('role', 'Chef de Projet');
    }

    /**
     * Determine whether the user can view the user.
     */
    public function view(User $user, User $model): bool
    {
        return $user->roles->contains('role', 'Chef de Projet') || $model->id === Auth::id();
    }

    /**
     * Determine whether the user can create user.
     */
    public function create(User $user): bool
    {
        return $user->roles->role == 'Chef de Projet';
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(User $user, User $model): bool
    {
        return $user->roles->role == 'Chef de Projet';
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(User $user, User $model): bool
    {
        return $user->roles->role == 'Chef de Projet';
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(User $user, User $model): bool
    {
        return $user->roles->role == 'Chef de Projet';
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(User $user, User $model): bool
    {
        return $user->roles->role == 'Chef de Projet';
    }

    /**
     * Undocumented function
     *
     * @param User $user
     * @param User $model
     * @return boolean
     */
    public function importCsv(User $user): bool
    {
        return $user->roles->contains('role', 'Chef de Projet');
    }
}
