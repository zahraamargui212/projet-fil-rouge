IndexedDB est une base de données orientée objet qui fonctionne directement dans le navigateur web.

Elle permet aux développeurs de stocker des données de manière persistante et structurée localement, ce qui signifie que les données sont sauvegardées sur l'ordinateur de l'utilisateur plutôt que sur un serveur distant.

Le concept de base d'IndexedDB repose sur des magasins d'objets. Un magasin d'objets est similaire à une table dans une base de données relationnelle. Chaque objet stocké dans un magasin d'objets a une clé unique qui permet de le récupérer facilement. Les objets peuvent être stockés sous forme de paires clé-valeur, où la clé est une chaîne de caractères et la valeur peut être n'importe quel objet JavaScript.

IndexedDB utilise des transactions pour garantir l'intégrité des données. Une transaction est un ensemble d'opérations effectuées sur la base de données. Par exemple, vous pouvez ouvrir une transaction, récupérer des objets à partir d'un magasin d'objets, les modifier et les enregistrer à nouveau.

Le modèle de base qu'IndexedDB utilise est le suivant :

    Ouvrir une base de données.
    Créer un objet de stockage dans la base de données.
    Démarrer une transaction, et faire des requêtes pour faire quelques opérations sur des bases de données, comme ajouter, ou récupérer des données.
    Attendre que l'exécution soit terminée, en écoutant le bon type d'événement DOM.
    Faire quelque chose avec les résultats (qui peuvent être trouvés dans l'objet de la requête).

