import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    // component: () => import('layouts/MainLayout.vue'),
    component: () => import('pages/LoginApi.vue'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },

  {
    path: '/camp',
    component: () => import('pages/CampagneApi.vue'),
  },

  {
    path: '/login',
    component: () => import('pages/LoginApi.vue'),
  },
  {
    path: '/campaign/:campaignId',
    name: 'campaign-details', 
    component: () => import('pages/CampaignApiShow.vue'),
  },
];

export default routes;
