DROP TRIGGER IF EXISTS change_status_auditor;

DELIMITER $$
CREATE TRIGGER change_status_auditor
BEFORE UPDATE ON campaigns
FOR EACH ROW
BEGIN
    IF NEW.auditor_id = OLD.creator_id THEN
        SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = 'Impossible d\'auditer une campagne que vous avez créé !.';
    END IF;
END $$

DELIMITER ;