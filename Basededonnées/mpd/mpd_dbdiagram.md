`dbdiagram.io`
```

Table "users" {
  "id" BIGINT [pk, not null, increment]
  "name" VARCHAR(255) [not null]
  "password" VARCHAR(255) [not null]
  "remember_token" VARCHAR(100)
  "created_at" TIMESTAMP
  "updated_at" TIMESTAMP
}

Table "roles" {
  "id" BIGINT [pk, not null, increment]
  "role" VARCHAR(255) [not null]
}

Table "sites" {
  "id" BIGINT [pk, not null, increment]
  "name" VARCHAR(255) [not null]
  "adress" VARCHAR(255) [not null]
}

Table "roles_users" {
  "role_id" BIGINT [not null]
  "user_id" BIGINT [not null]
}

Table "campaigns" {
  "id" BIGINT [pk, not null, increment]
  "date_start" DATE [not null]
  "date_end" DATE [not null]
  "name" VARCHAR(255) [not null]
  "status" VARCHAR(255) [not null]
  "auditor_id" BIGINT
  "creator_id" BIGINT [not null]
  "site_id" BIGINT [not null]
}

Table "types" {
  "id" BIGINT [pk, not null, increment]
  "nom" VARCHAR(255) [not null]
}

Table "constructors" {
  "id" BIGINT [pk, not null, increment]
  "name" VARCHAR(255) [not null]
}

Table "models" {
  "id" VARCHAR(255) [not null]
  "name" VARCHAR(255) [not null]
  "creation_year" INT [not null]
  "status" VARCHAR(255) [not null]
  "has_electro" INT [not null]
  "constructor_id" BIGINT [not null]
  "type_id" BIGINT [not null]
  "compose" VARCHAR(255)
}

Table "pieces" {
  "id" BIGINT [pk, not null, increment]
  "date_mer" INT [not null]
  "status_fonctionnel" VARCHAR(255) [not null]
  "material_id" VARCHAR(255)
  "model_id" VARCHAR(255) [not null]
}

Table "materials" {
  "id" VARCHAR(255) [not null]
  "model_id" VARCHAR(255) [not null]
  "site_id" BIGINT [not null]
  "constructor_id" BIGINT [not null]
  "description" TEXT [not null]
}

Table "compatible_models" {
  "model_A" VARCHAR(255) [not null]
  "model_B" VARCHAR(255) [not null]
}

Table "modelstemp" {
  "id" VARCHAR(255) [not null]
  "name" VARCHAR(255) [not null]
  "creation_year" INT [not null]
  "status" VARCHAR(255) [not null]
  "has_electro" INT [not null]
  "piece_associes" VARCHAR(255)
  "constructor_id" BIGINT [not null]
  "type_id" BIGINT [not null]
  "compose" VARCHAR(255)
}


Table "campaigns_pieces" {
  "id" BIGINT [pk, not null, increment]
  "piece_id" BIGINT [not null]
  "campaign_id" BIGINT [not null]
  "Question_1" VARCHAR(255) [not null]
  "Question_2" VARCHAR(255) [not null]
  "Question_3" VARCHAR(255) [not null]
  "Question_4" VARCHAR(255) [not null]
  "Question_5" VARCHAR(255) [not null]
  "Question_6" VARCHAR(255)
}

Ref:"roles"."id" < "roles_users"."role_id" [update: cascade, delete: cascade]

Ref:"users"."id" < "roles_users"."user_id" [update: cascade, delete: cascade]

Ref:"users"."id" < "campaigns"."auditor_id"

Ref:"users"."id" < "campaigns"."creator_id"

Ref:"sites"."id" < "campaigns"."site_id"

Ref:"constructors"."id" < "models"."constructor_id"

Ref:"types"."id" < "models"."type_id"

Ref:"sites"."id" < "materials"."site_id"

Ref:"constructors"."id" < "materials"."constructor_id"

Ref:"constructors"."id" < "modelstemp"."constructor_id"

Ref:"types"."id" < "modelstemp"."type_id"

Ref:"pieces"."id" < "campaigns_pieces"."piece_id"

Ref:"campaigns"."id" < "campaigns_pieces"."campaign_id"


Ref: "compatible_models"."model_A" < "models"."id"

Ref: "models"."id" < "compatible_models"."model_B"

Ref: "models"."id" < "pieces"."model_id"

Ref: "modelstemp"."id" < "modelstemp"."compose"
```

